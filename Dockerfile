# Use Python 3.10-bullseye as the base image
FROM python:3.10-bullseye

# Set the working directory
WORKDIR /app

# Copy only the requirements file to leverage Docker cache
COPY requirements.txt /app/

# Install dependencies
RUN pip install --no-cache-dir -r requirements.txt

# Copy the rest of the application files to the container
COPY . /app

# Command to run the application
CMD ["python", "main.py"]
