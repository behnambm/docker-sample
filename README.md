# Project Setup Guide for Beginners

This guide will help you set up and run a Flask app with a MySQL database and Redis cache using Docker.

### Prerequisites
Make sure you have the following installed on your machine:

- [Docker](https://docs.docker.com/engine/install/)

  - NOTE: Using Proxy To connect installation servers:
          If you are from Iran and having issues with the installation process you can use [Shecan](https://shecan.ir/tutorials/) to access the docker without using any VPN.

- [Git](https://git-scm.com/downloads)
   Tp Downlaod Git Software and Manage Code and Branches

- [Docker Hub](https://hub.docker.com/)
   It is necessary to create account and signin docker hub when installing software and updates.

- [GitHub](https://github.com/)
  To have online project(source code) repository, we will create an account in GitHub and Upload our project Into it.

# Architecture Overview
![arch overview](arch.png)


# How to run
### Step 1: Clone the Repository from server to your client
```bash 
git clone https://gitlab.com/behnambm/docker-sample.git
this will download latest version of project into your working area (personal computer)
```
### Go to Sample Downloaded Folder using cd command
```bash 
cd docker-sample
```

### Step 2: Build and Run Docker Containers, in this step considers ### Prerequisites
```bash 
docker compose up --build
```

This command will first start downloading the required images and the creates images 
After creating images it will create project container and run it.

This command build and update all necessary requirement of project.

### Step 3: Access the Flask Web App
Visit http://localhost:5000 in your web browser. 

When running, it will check database access and connection to database.
it will be possible to store message from web application into MySql Container. the MySql will store data to volume persistently.
second time, when your run Web app, it will retrieve data from database and shows its persistency.

---

# Development guide 

## Step 1: Define Database Models
Start by having an overall idea of your database structure. 
Define models to represent entities and their relationships, making it easier to interact with the database.

### model.py and utils.py 
Here I have a `Message` model in `model.py` which represents the data model.
### this model import SQLAlchemy
### from flask_sqlalchemy import SQLAlchemy

```python 
class Messages(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    text = db.Column(db.Text)
    created_at = db.Column(db.DateTime, default=db.func.now(), nullable=False)
```
Read more:
 - [Flask-SQLAlchemy](https://flask-sqlalchemy.palletsprojects.com/en/3.0.x/quickstart/)

### Database Checking methods in utils.py
1. healthcheck()
2. ping_redis_connection()
3. redis_connection
4. ping_database
5. connect_to_mysql
6. get_mysql_uri


## Step 2: Design APIs
Create a comprehensive design for your APIs. 
Clearly define endpoints, request/response formats, and error handling.

There are 3 main endpoints in this project:
- `index` renders the main page
- `add-message` adds a new `Message` to the database
- `healthcheck` is used for internal monitoring services in order to check the health state of the service

API's are located in the `main.py` file.


## Step 3: Dockerize the Web Application
Craft a `Dockerfile` for your project. 
This file contains instructions for building a Docker image of your application. 
Ensure it includes dependencies, configurations, and steps needed to set up the environment within a container.

1. A service should be created with a specified `Dockerfile` in order to be able to build the image from scratch.
2. Set the required ENVs based on the applications needs.
3. Copy required File Into App Directory 
4. Publish required ports

## Step 4: Compose docker-compose.yml
Create a `docker-compose.yml` file to define all the services your application requires. Specify networks for services to communicate with each other effectively. Utilize volumes to persist important data, ensuring that data is not lost when containers are recreated.

Here is the used network in this project:
```yaml 
networks:
  internal-net:
```
this network makes sure that the services which are connected are able to see and talk to each other.

In order to have persistent data we use Docker volumes:
```yaml 
volumes:
  mysql_data:
```
this piece will create a new volume.

Read more:
 - [Docker Network](https://docs.docker.com/network/)


```yaml 
volumes:
  - mysql_data:/var/lib/mysql
```
here the `mysql_data` will be mounted to the container and `mysql` will write its data on it and the data will persis on the hosts 
file system.

Read more:
 - [Docker Volumes](https://docs.docker.com/storage/volumes/)



## Step 5: Version Control with Git
Use `Git` to track changes in your project. Regularly commit your code and write meaningful commit messages. 

## Step 6: Collaborate with GitHub/GitLab
Leverage platforms like `GitHub` or `GitLab` to share your source code with others. 