import logging
from json import dumps, loads


def save_messages_to_redis(redis_conn, messages):
    try:
        messages_dict = [message.to_dict() for message in messages]
        messages_json = dumps(messages_dict)

        redis_conn.set('messages', messages_json)

        return True
    except Exception as e:
        logging.error(f"save_messages_to_redis: {e}")
        return False


def get_messages_from_redis(redis_conn):
    try:
        messages_json = redis_conn.get('messages')
        if messages_json:
            return loads(messages_json)
        else:
            return None
    except Exception as e:
        logging.info(f"get_messages_from_redis: {e}")
        return None


def invalidate_cache(redis_conn):
    logging.debug(f"invalidating cache")
    redis_conn.delete('messages')
