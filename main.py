import os
from datetime import datetime

from flask import Flask, render_template, request, jsonify, redirect

import cache
import utils
from model import db, Messages, config_database

app = Flask(__name__)
config_database(app)


def get_all_messages():
    # Retrieve all messages from Redis
    redis_conn = utils.redis_connection()
    cached_messages = cache.get_messages_from_redis(redis_conn)

    if cached_messages:
        app.logger.info("Cache: HIT")
        return cached_messages
    else:
        app.logger.info("Cache: MISS")

        # Retrieve messages from the database
        messages = Messages.query.all()

        # Save messages to Redis for future use
        cache.save_messages_to_redis(redis_conn, messages)

        return messages


@app.route('/')
def index():
    db_connection = utils.connect_to_mysql()
    cache_connection = utils.redis_connection()

    return render_template(
        'index.html',
        db_conn_healthy=utils.ping_database(db_connection),
        cache_conn_health=utils.ping_redis_connection(cache_connection),
        messages=get_all_messages(),
    )


@app.route('/add-message', methods=['GET', 'POST'])
def add_message():
    if request.method == 'POST':
        # Get the message from the form submission
        message_text = request.form.get('message')

        if message_text:
            # Create a new Messages object and add it to the database
            new_message = Messages(text=message_text, created_at=datetime.now())
            db.session.add(new_message)
            db.session.commit()

            cache.invalidate_cache(utils.redis_connection())

            return redirect('/')  # Redirect to the root route
        else:
            return jsonify({'error': 'Message cannot be empty'})
    else:
        return redirect('/')


@app.route("/healthcheck")
def health_check():
    if utils.healthcheck():
        return jsonify({'status': 'ok'}), 200
    else:
        return jsonify({'status': 'error'}), 500


if __name__ == '__main__':
    app.run(
        debug=True,
        host="0.0.0.0",
        port=5000,
    )
