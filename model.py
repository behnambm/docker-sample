from flask_sqlalchemy import SQLAlchemy

import utils

db = SQLAlchemy()


class Messages(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    text = db.Column(db.Text)
    created_at = db.Column(db.DateTime, default=db.func.now(), nullable=False)

    def to_dict(self):
        return {'id': self.id, 'text': self.text, 'created_at': self.created_at.isoformat()}


def config_database(app):
    app.config['SQLALCHEMY_DATABASE_URI'] = utils.get_mysql_uri()

    # This is a feature that mostly gets used in development phase. It can be disabled if not needed explicitly.
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

    db.init_app(app)

    with app.app_context():
        db.create_all()
