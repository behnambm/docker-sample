import logging
import os

import mysql.connector
import redis


def get_mysql_uri():
    host = os.getenv('MYSQL_HOST')
    user = os.getenv('MYSQL_USER')
    password = os.getenv('MYSQL_PASSWORD')
    database = os.getenv('MYSQL_DATABASE')

    return f"mysql+pymysql://{user}:{password}@{host}/{database}"


def connect_to_mysql():
    # Get MySQL connection details from environment variables
    host = os.getenv('MYSQL_HOST')
    user = os.getenv('MYSQL_USER')
    password = os.getenv('MYSQL_PASSWORD')
    database = os.getenv('MYSQL_DATABASE')

    # Check if any of the required environment variables is missing
    if not all([host, user, password, database]):
        raise ValueError("Missing one or more MySQL environment variables.")

    # Establish a connection to the MySQL database
    try:
        connection = mysql.connector.connect(
            host=host,
            user=user,
            password=password,
            database=database
        )

        return connection

    except mysql.connector.Error as err:
        logging.error(f"connect_to_mysql: {err}")
        return None


def ping_database(connection):
    try:
        # Use the connection to ping the database
        connection.ping(reconnect=True)
        return True

    except mysql.connector.Error as err:
        logging.error(f"ping_database: {err}")
        return False


def redis_connection():
    host = os.getenv('REDIS_HOST')
    port = int(os.getenv('REDIS_PORT'))
    db = int(os.getenv('REDIS_DB'))
    return redis.StrictRedis(host=host, port=port, db=db)


def ping_redis_connection(redis_conn):
    try:
        return redis_conn.ping()
    except redis.RedisError as e:
        logging.error(f"ping_redis_connection: {e}")
        return False


def healthcheck():
    # Check MySQL database connection
    db_connection = connect_to_mysql()
    db_conn_healthy = ping_database(db_connection)

    # Check Redis cache connection
    cache_connection = redis_connection()
    cache_conn_health = ping_redis_connection(cache_connection)

    return db_conn_healthy and cache_conn_health
